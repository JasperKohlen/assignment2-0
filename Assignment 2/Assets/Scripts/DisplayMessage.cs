﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMessage : MonoBehaviour
{
    [SerializeField] private Text textMessage;
    [SerializeField] private HealthDisplay healthDisplay;

    public void DisplayDeathMessage()
    {
        if (healthDisplay.health == -1)
        {
            textMessage.text = "You died";
        }
        else
        {
            textMessage.text = "";

        }
    }
}
