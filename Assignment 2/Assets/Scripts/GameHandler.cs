﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    [SerializeField] private HealthDisplay healthDisplay;

    public void LowerHealth()
    {
        healthDisplay.TakeDamage();
    }
}
