﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    public int health = 5;
    public Text healthText;

    public void TakeDamage()
    {
        healthText.text = $"Health : {health}";

        health--;
    }

    void Update()
    {
        if (health == -1)
        {
            health = 5;
        }
    }
}
